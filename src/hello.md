# Hello Again

This is regular text generated from regular Markdown.

Foliant doesn’t force any *special* Markdown flavor.

<blockdiag caption="High-quality diagram" format="svg">
  blockdiag {
    A -> B -> C -> D;
    A -> E -> F -> G;
  }
</blockdiag>
